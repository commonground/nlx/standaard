# NLX standaard

[toc]

## Introductie

NLX is een stelsel van software en afspraken waarmee (overheids)organisaties koppelingen naar 
interne en externe gegevensbronnen op een laagdrempelige, snelle en veilige wijze kunnen inrichten. 
Hiermee wordt het data-bij-de-bron principe van Common Ground bereikbaar. 
NLX helpt datasilo’s te doorbreken en draagt bij aan een flexibeler datalandschap, 
waardoor beter kan worden ingespeeld op veranderende maatschappelijke vraagstukken. 

Dat data-bij-de-bron principe is een van de pijlers van Common Ground. Het zorgt ervoor dat data alleen 
bij de bron bestaat en daar onderhouden wordt. Dat zorgt voor een overzichtelijk datalandschap en een 
verhoging van datakwaliteit. Gevolg van dat principe is een groei van het aantal koppelingen tussen data-afnemers 
en databronnen. Applicaties werken namelijk niet meer met een lokale kopie van gegevens, maar halen de data 
rechtstreeks bij de bron. Die koppelingen zitten in laag 3 van het 5-lagen model van Common Ground: 
de integratielaag. NLX is het connectiviteitsdeel van de integratielaag. Het zorgt ervoor dat koppelingen 
beheersbaar en betaalbaar kunnen worden georganiseerd.

NLX combineert gateway functionaliteit, bestaande technologie, met een uniforme wijze om koppelingen 
te maken en beheren. Het project is geïnspireerd door het succes van de Estlandse X-Road, 
vandaar de naam NL-X (Nederlandse X-Road). De code wordt open source ontwikkeld, zodat gemeenten, andere overheden, 
ketenpartners en marktpartijen de mogelijkheid hebben om ontwikkelingen te volgen en mee te ontwikkelen. 
Iedere deelnemer aan het NLX ecosysteem identificeert zich met een PKI-Overheid certificaat en gaat 
daarna op uniforme en gestandaardiseerde wijze om met koppelingen. 
Zo is de uitwisseling van data transparant, snel en veilig georganiseerd. Een koppeling leggen is een 
kwestie van minuten, in plaats van weken, en het uitgeven van organisatiecertificaten aan derden is verleden tijd.

### Versie

1.0 Eerste versie van deze standaard

### Huidige ontwikkelingen

- De [git-repository van de referentieimplementatie](https://gitlab.com/commonground/nlx/nlx)
- De [sprintbacklog](https://gitlab.com/groups/commonground/nlx/-/boards/1860837?milestone_title=%23started).
- De [productbacklog](https://gitlab.com/groups/commonground/nlx/-/boards/2015075?milestone_title=None)

### Begrippenlijst

Begrip | Uitleg
--- | --- 
API | Application Programming Interface. Software die verschillende andere software met elkaar verbindt door voor te schrijven hoe die communicatie er uit ziet. Welke vraag kan je stellen, welk antwoord kan je verwachten en hoe zijn die vraag en dat antwoord gestructureerd.
Bron eigenaar | Een organisatie (kan intern of extern zijn) die een API aanbiedt op het NLX ecosysteem.
Delegator | De eigenaar van een opdracht
Delegatee | De onvanger van een opdracht
Directory | Het overzicht van beschikbare API's in en deelnemende organisaties aan het NLX ecosysteem.
Ecosysteem | Een samenhangend geheel van losse elementen welke elkaar beinvloeden. In de context van NLX het geheel aan organisaties wat via de NLX componenten aan elkaar verbonden is via dezelfde directory. 
Endpoint | De locatie waarop bijvoorbeeld een API benaderbaar is.
Inway | Het NLX component via wat API's benaderbaar zijn voor het NLX ecosysteem. Dit component is publiek benaderbaar.
mTLS | Mutual Transport Layer Security: een verbinding waarbij tweezijdige authenticatie plaats gevonden heeft.
Opdracht (delegatie) | Een set aan toegang tot API's van de opdrachtgever(Delegator) aan een opdrachtnemer(Delegatee) waarmee de opdrachtnemer tijdelijk namens de opdrachtgever data mag ophalen bij een bron eigenaar.
Organisatie Inway | Het endpoint wat een organisatie gebruikt om toegangsverzoeken te ontvangen.
Outway | Het NLX component via wat een client data ophaalt bij een API over het NLX ecosysteem.
Service | Een API die wordt aangeboden op NLX.
Toegangsbewijs (Access proof) | Het bewijs van een goedgekeurd toegangsverzoek.
Toegangsverzoek (Access request) | Het verzoek van een organisatie aan een bron eigenaar om toegang te krijgen tot de API.
Toegangsverlening (Access grant) | Een toegangsverlening voor een toegansverzoek

## NLX ecosysteem

Afbeelding van blauwe blokken met een benoemen van de componenten.
In die afbeelding ook benoemen hoe ze met elkaar communiceren.

## Authenticatie en autorisatie

### Authenticatie

NLX gebruikt [X.509](https://en.wikipedia.org/wiki/X.509) certificaten om de identiteit van een organisatie vast te leggen. 
Elk certificaat moet ondertekend zijn door de root CA die gebruikt wordt door het NLX-netwerk. 

In het certificaat worden de volgende velden gebruikt om identiteit van een organisatie te bepalen:
- Subject SerialNumber ([see specification](https://www.rfc-editor.org/rfc/rfc5280#section-4.1.2.2))
- Subject OrganizationName

Waarbij het attribuut Subject SerialNumber binnen de NLX-componenten wordt gebruikt als primaire identificatie sleutel. Het attribuut Subject OrganisationName kan worden gebruikt om de identiteit van de organisatie menselijk leesbaar te maken, maar mag niet gebruikt worden als primaire identificatie sleutel.

#### Certificaten

De identiteit van een organisatie binnen een NLX ecosysteem is gebaseerd op een certificatenstelsel.
De organisatie die de certificaten beheert en uitgeeft, draagt zorg voor de indentificatie van deelnemende
organisaties. Een certificaat moet minimaal de volgende velden bevatten:

- subject serial number
- organisatie naam

#### mTLS

Een Outway en Inway zetten samen een zogenaamde mTLS verbinding op wat betekent dat deze verbinding met beide certificaten beveiligd wordt.
De firewall mag daarom geen certificate offloading doen.

### Authorisatie

Om je te authorizeren voor een service maakt NLX gebruik van toegangsverzoeken.
Toegang vraag je altijd aan op basis van een Public Key die is gekoppeld aan een Outway.

Als een toegangsverzoek is goedgekeurd, kan je de service bevragen met de Outway waarvoor je toegang hebt aangevraagd.

Een organisatie die meerdere Outways heeft met verschillende certificaten moet dus per certificaat toegang
krijgen om een bron te bevragen.

Dit concept heet ook wel public key pinning.

#### Granulariteit autorisatie
- Open Policy Agent

### Delegatie

Het is mogelijk om de toegang die je hebt tot een service, te delegeren naar een Outway van een andere organisatie.
Dat kan door gebruik te maken van 'Opdrachten'. Deze functionaliteit kan handig zijn voor bv. SaaS-leveranciers die services bevragen voor meerdere gemeenten.

Om een service te bevragen met behulp van een opdracht, zijn twee HTTP-headers nodig.
Namelijk `X-NLX-Request-Delegator` en `X-NLX-Request-Order-Reference`.

Voorbeeld van een HTTP-request die gebruik maakt van delegatie:

```bash
curl -H "X-NLX-Request-Delegator: 12345678901234567890" -H "X-NLX-Request-Order-Reference: test" https://my-nlx-outway.nl/12345678901234567891/basisregister-fictieve-kentekens/
````


## Componenten

### Outway

Via een Outway kan een organisatie services op het NLX-netwerk bevragen.


### Inway

Via een Inway kan een organisatie services aanbieden op het NLX-netwerk.

### Directory

Centraal component binnen een NLX-netwerk. Organisaties kunnen hier hun aangeboden services aanmelden.
Het geeft een overzicht aan gebruikers van welke services beschikbaar zijn per organisatie.

Outways maken gebruik van de directory om te weten hoe ze een request naar een bepaalde service bij de juiste 
organisatie terecht kunnen laten komen.

### Management UI

Grafische interface om je NLX-installatie te beheren. Gemaakt voor systeembeheerders.
Het geeft inzicht in je services, toegang, audit logs, etc.

De Management UI maakt gebruik van de Management API.

### Transaction log

De transacties die via het NLX-netwerk lopen zijn in te zien in de transaction log.
Deze UI is te vinden in NLX Management. Er is ook een transaction log API beschikbaar om data uit te lezen.

### Databases

- PostgreSQL (voor de Management API en de Directory API)

## Versies externe libraries en protocollen

- TLS versie >= 1.2
- HTTP
- PostgreSQL
- gRPC

## Core processen

### Toegang aanvragen

```plantuml
!include https://gitlab.com/commonground/nlx/nlx/-/raw/ce384494a0b105b8f0ae43cc864c1b7ccd3cf669/technical-docs/sequence-diagrams/request-access.puml
```

### Delegatie

### Service bevragen

### Service aanbieden

## Ondersteunende processen

### Scheduler

```plantuml
!include https://gitlab.com/commonground/nlx/nlx/-/raw/ce384494a0b105b8f0ae43cc864c1b7ccd3cf669/technical-docs/sequence-diagrams/access-request-synchronization.puml
```

- https://logius-standaarden.github.io/API-Design-Rules/
